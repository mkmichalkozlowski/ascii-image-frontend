import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './App.css';

import Header from './components/Header.js'
import MainPage from './components/MainPage.js'
import ImagePage from './components/ImagePage.js'
import Footer from './components/Footer.js'

const App = () => {

  var cards = []

  return <>
    <Header/>

    <Router>
      <Route exact path="/">
        <MainPage name={cards}/> 
      </Route>

      <Route path="/image/:id" component={ImagePage} />

    </Router>

    <Footer/>
  </>
}

export default App;
