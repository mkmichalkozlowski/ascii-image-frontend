import React, { useState } from 'react';
import axios from 'axios';

const AddNewForm = (props) => {

    const [inputs, setInputs] = useState({})

    const [file, setFile] = useState({})

    const handleInputChange = (event) => {
        event.persist();
        setInputs(inputs => ({...inputs, [event.target.name]: event.target.value}));
    }

    const handleFileInputChange = (event) => {
        event.persist();
        setFile(event.target.files[0]);
    }

    const onClickHandler = (event) => {
        event.preventDefault();

        const data = new FormData()
        data.append('file', file)
        data.append('title', inputs.title)

        axios.post("http://localhost:8080/api/image/add", data, { // receive two parameter endpoint url ,form data 
        })
        .then(res => { // then print response status
            props.callback();
        })
    }

return <>
    <form>
        <div className="form-group">
            <label htmlFor="entryName">Entry name</label>
            <input type="text" className="form-control" id="entryName" name="title" aria-describedby="entryNameHelp" placeholder="Enter entry name" 
                onChange={(event) => handleInputChange(event)}/>
            <small id="entryNameHelp" className="form-text text-muted">Name will be public.</small>
        </div>
        <div className="form-group">
            <label htmlFor="inputFile">File</label>
            <input type="file" className="form-control-file" id="inputFile" placeholder="File" name="file"
            onChange={(event) => handleFileInputChange(event)}/>
        </div>
        <button type="submit" className="btn btn-primary" onClick={(event) => onClickHandler(event)}>Submit</button>
    </form>
    </>
}

export default AddNewForm;