import React, {useState, useEffect} from 'react';
import { Redirect } from 'react-router';
import axios from 'axios';
import ColoredLine from './ColoredLine';

const ImagePage = (props) => {

const [card, setCard] = useState([]);

const [redirectToHome, setRedirectToHome] = useState(false);

  const fetchCard = async () => {
    const response = await axios.get("http://localhost:8080/api/image/" + props.match.params.id);
    setCard([response.data])
  }

  useEffect(() => { fetchCard() }, [])

    return (
    <>
        {redirectToHome ? <Redirect to="/" /> : null}

        <main role="main">
            <div className="container">
                <div className="row my-4">
                    <button type="button" className="btn btn-primary" onClick={() => setRedirectToHome(true)}>Go back to home</button>
                </div>
            </div>
            <div className="container w-100 mw-100">
                <div className="row text-monospace">
                    <div className="col-md-auto mx-auto small-line">
                        {
                            card.map(item => 
                                <>
                                    <h2>Image Page {item.title}</h2>
                                    <h3>{item.date}</h3>
                                    {
                                        item.coloredText.map(line =>
                                            <p className={"my-0" + (line.length > 150 ? " text-small" : "")}><ColoredLine line={line}/></p>
                                        )
                                    }
                                </>
                            )
                        }
                    </div>
                </div>
            </div>
        </main>
    </>
    )
}

export default ImagePage