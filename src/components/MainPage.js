import React, {useState, useEffect} from 'react';

import axios from 'axios';

import ImageCard from './ImageCard.js'
import AddNewForm from './AddNewForm.js'

const MainPage = (props) => {

  const [cards, setCards] = useState(props.name);

  const fetchCards = async () => {
    const response = await axios.get("http://localhost:8080/api/image/");
  
    setCards(response.data)
  }

  useEffect(() => { fetchCards(cards) }, cards)

  return (
    <main role="main">
      <section className="jumbotron text-center">
        <div className="container">
          <h1 className="jumbotron-heading">Ascii image gallery</h1>
          <p className="lead text-muted">Example project to convert images into asci art.</p>
        </div>
      </section>

      <div className="album py-5 bg-light">
        <div className="container">

          <div className="row mb-4">
            <div className="accordion col-md-12" id="accordionExample">
              <div className="border rounded-bottom px-0">
                <div className="card-header" id="headingOne">
                  <h2 className="mb-0">
                    <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                      Add new image +
                    </button>
                  </h2>
                </div>

                <div id="collapseOne" className="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                  <div className="card-body col-md-8">
                    <AddNewForm callback={() => fetchCards()}/>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="card-columns col-md-12">
              {
                cards.map(card => (
                  <ImageCard value="http://placekitten.com/150/100" key={card.id} id={card.id} title={card.title} callback={() => fetchCards()}/>
                ))
              }
            </div>
          </div>

        </div>
      </div>
    </main>
  )
}

export default MainPage;