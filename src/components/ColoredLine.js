import React, {useState, useEffect} from 'react';

const ColoredLine = (line) => {
    
    const toColor = (num) => {
        num >>>= 0;
        var b = num & 0xFF,
            g = (num & 0xFF00) >>> 8,
            r = (num & 0xFF0000) >>> 16,
            a = ( (num & 0xFF000000) >>> 24 ) / 255 ;
        return "rgba(" + [r, g, b, a].join(",") + ")";
    }

    return line.line.map(char =>
        <span style={{color: toColor(char.color)}}>{char.character}</span>
    )
}

export default ColoredLine