import React, {useState, useEffect} from 'react';
import { Redirect } from 'react-router';
import axios from 'axios';

const ImageCard = ({ value, id, title, callback }) => {

    const [redirect, setRedirect] = useState(false);

    const handleOnClick = () => {
        setRedirect(true);
    }

    const handleOnDelete = () => {
        axios.delete("http://localhost:8080/api/image/" + id)
        .then(res => { // then print response status
            console.log(res)
            callback()
        })
    }

    return (
        <>
        {redirect ? <Redirect to={"/image/" + id} /> : null}
    
            <div className="card mb-4 box-shadow">
                <img className="card-img-top" src={"http://localhost:8080/api/image/" + id + "/image"} alt="Card cap"/>
                <div className="card-body">
                    <p className="card-text">{title}</p>
                    <div className="d-flex justify-content-between align-items-center">
                    <div className="btn-group">
                        <button type="button" className="btn btn-sm btn-outline-secondary" onClick={handleOnClick}>View</button>
                        <button type="button" className="btn btn-sm btn-outline-secondary" onClick={handleOnDelete}>Delete</button>
                    </div>
                    <small className="text-muted">9 mins</small>
                    </div>
                </div>
            </div>
        </>
    )
}


export default ImageCard;